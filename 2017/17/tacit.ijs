#!/usr/bin/env jconsole
NB. Solution to https://adventofcode.com/2017/day/17 in J
maxpos=: i.>./
idx=: 1+#|394+maxpos
iterate=: (idx{.]), #, idx}.]
ring=: iterate^:2017 ,0
echo (1+maxpos ring) { ring
NB. exit''
