#!/usr/bin/env jconsole
NB. Solution to https://adventofcode.com/2017/day/17 in J
maxpos=: (i. >./)
idx =: 3 : '1+(#y)|394+maxpos y'
iterate =: 3 : '((idx y) {. y), (#y), (idx y) }. y'
ring =: iterate ^: 2017 (1$0)
echo (1+maxpos ring) { ring
NB. exit''
