#lang racket

(define (char-counts str)
  (let ((chars (string->list str)))
    (map length (group-by identity chars))))

(define (has-n-of-a-char n)
  (λ (s) (if (member n (char-counts s))
             #t
             #f)))

(define rows (file->lines "input.txt"))

(* (count identity (map (has-n-of-a-char 2) rows))
   (count identity (map (has-n-of-a-char 3) rows)))