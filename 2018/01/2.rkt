#lang scheme
(require racket/generator)
(require racket/set)

(define raw-data (file->list "input.txt"))

(define datum (sequence->repeated-generator raw-data))

(define freq (generator ()
                        (let loop ([cur 0])
                          (yield cur)
                          (loop (+ cur (datum))))))

(define (first-repeated-freq freqs next)
  (if (set-member? freqs next)
      next
      (first-repeated-freq (set-add freqs next) (freq))))

(first-repeated-freq (set) (freq))