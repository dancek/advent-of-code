#lang racket
(require threading)

(define guard-data
  (~>> (file->lines "input.txt")
       (sort _ string<?)
       (string-join _ "\n")
       (regexp-split #rx"\\[[^]]*] Guard #")
       (filter-map (curry regexp-match #rx"([0-9]*) begins shift\n(.*)"))
       (map rest)
       (map (λ (xs) (cons (first xs)
                          (~> (second xs)
                              (string-split "\n")
                              (map (λ (s) (substring s 15 17)) _)))))
       (map (curry map string->number))
       (group-by first)
       (map (λ (xs) (cons (first (first xs))
                          (~>> xs
                               (map rest)
                               flatten))))))

(define (sleep-table-internal table xs)
  (if (empty? xs)
      table
      (let ((start (first xs))
            (end (second xs))
            (rst (rest (rest xs))))
        (sleep-table-internal
         (map (λ (idx x)
                (if (<= start idx (sub1 end))
                    (add1 x)
                   x))
              (range 60)
              table)
         rst))))

(define (sleep-table guard)
  (sleep-table-internal (make-list 60 0) (rest guard)))

(define (sleep-amount guard)
  (foldl + 0 (sleep-table guard)))

(define (most-slept-minute guard)
  (let ((tbl (sleep-table guard)))
    (index-of tbl (apply max tbl))))

; A
(let* ((amounts (map sleep-amount guard-data))
       (max-idx (index-of amounts (apply max amounts)))
       (max-guard (list-ref guard-data max-idx))
       (id (first max-guard))
       (minute (most-slept-minute max-guard)))
  (list id minute (* id minute)))

; B
(let* ((tbls (map sleep-table guard-data))
       (flat (flatten tbls))
       (max-sleep (apply max flat))
       (max-idx (index-of flat max-sleep))
       (id (first (list-ref guard-data (quotient max-idx 60))))
       (minute (remainder max-idx 60)))
  (list id minute (* id minute)))