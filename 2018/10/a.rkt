#lang racket
(require threading)

(define input (~>> "input.txt"
                   file->lines
                   (map (curry regexp-match* #rx"-?[0-9]+"))
                   (map (curry map string->number))))

(define (step data)
  (map (match-lambda
        [(list x y dx dy)
         (list (+ x dx) (+ y dy) dx dy)])
       data))

(define (area data)
  (let ([xs (map first data)]
        [ys (map second data)])
    (let ([x1 (apply min xs)]
          [x2 (apply max xs)]
          [y1 (apply min ys)]
          [y2 (apply max ys)])
      (list (- x2 x1 -2) ; usability; make room for newline
            (- y2 y1 -1)
            x1
            y1))))

; run this and guess the iteration
;'(10618 (69 19 132 187))
;'(10619 (61 9 137 192))
;'(10620 (69 19 132 187))
(define (log-steps data n)
  (when (< 10500 n 10800)
    (println (list n (area data))))
  (if (> n 11000)
      #t
      (log-steps (step data) (add1 n))))

(define (steps data n)
  (if (zero? n)
      data
      (steps (step data) (sub1 n))))

(define (print_ data)
  (match-let ([(list w h dx dy) (area data)])
    (when (< w 120)
      (let ([canvas (make-string (* w h) #\.)])
        (for ([i (range h)])
          (string-set! canvas (sub1 (* (add1 i) w)) #\newline))
        (for ([datum data])
          (let ([x (- (first datum) dx)]
                [y (- (second datum) dy)])
            (string-set! canvas
                         (+ x (* y w))
                         #\#)))
        (display canvas)))))

; A
(define msg-data (steps input 10619))
(print_ msg-data)

; B look up ^ 10619