#lang racket
(require threading)

(define points
  (~>> "input.txt"
       file->lines
       (map (λ (s) (map string->number
                        (string-split s ", "))))))

(define minx (apply min (map first points)))
(define maxx (apply max (map first points)))
(define miny (apply min (map second points)))
(define maxy (apply max (map second points)))
(define width (- maxx minx))

(define (distance p1 p2)
  (+ (abs (- (first p1) (first p2)))
     (abs (- (second p1) (second p2)))))

(define (closest-index x y)
  (let* ([distances (map (curry distance (list x y)) points)]
         [min-dist  (apply min distances)]
         [min-idxs  (indexes-of distances min-dist)])
    (if (= 1 (length min-idxs))
        (first min-idxs)
        #f)))

(define closest-map
  (for*/list ([x (range (add1 minx) maxx)]
              [y (range (add1 miny) maxy)])
    (closest-index x y)))

(define border-indices
  (set-union
   (for*/set ([x (list minx maxx)]
              [y (range miny maxy)])
     (closest-index x y))
   (for*/set ([x (range minx maxx)]
              [y (list miny maxy)])
     (closest-index x y))
   (set #f)))

(define areas
  (~>> closest-map
       (filter (compose1 not (curry set-member? border-indices)))
       (group-by identity)
       (map length)))

; A
(apply max areas)

; B
(define (distance-sum x y)
  (foldl + 0 (map (curry distance (list x y)) points)))

(define sum-map
  (for*/list ([x (range (add1 minx) maxx)]
              [y (range (add1 miny) maxy)])
    (distance-sum x y)))

(count (curry > 10000) sum-map)